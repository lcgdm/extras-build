#!/bin/bash
#
# From the name of a Jenkins axis, e.g. epel-6-i386
# this script outputs the arch, e.g. i386
#
# Fabrizio Furano (furano@cern.ch)
# June 2015

arch=x86_64
if   [[ "$1" = *x86_64* ]]; then
	arch=x86_64
elif [[ "$1" == *i386* ]]; then
	arch=i386
else
        echo "Architecture in $1 not recognized"
        exit 255
fi

echo $arch

