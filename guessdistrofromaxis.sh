#!/bin/bash
#
# From the name of a Jenkins axis, e.g. epel-6-i386
# this script outputs the distro, e.g. epel-6
#
# Fabrizio Furano (furano@cern.ch)
# June 2015

dist=el6
if   [[ "$1" == epel-7* ]]; then
        dist=el7
elif [[ "$1" == epel-6* ]]; then
	dist=el6
elif [[ "$1" == epel-5* ]]; then
	dist=el5
elif [[ "$1" == alma-9* ]]; then
	dist=al9
else
        echo "Distribution in $1 not recognized"
        exit 255
fi

echo $dist

