from optparse import OptionParser
import os
import re

def pprint_list(l):
  for i in l:
    print i

def get_delete_list(repo_path, component_name, num_versions_kept=2):
  # hack for puppet-modules-lcgdm
  if component_name == 'puppet-modules-lcgdm':
    component_name = 'puppet-modules'

  cmd = "ls %s | grep -e '^%s.*\.rpm' | sort | tac"

  lines = map(lambda s:s.strip(), os.popen(cmd % (repo_path, component_name)).readlines())

  buckets = {}
  delete_list = []
  control_count = 0

  bucket_sort_regex = re.compile("(.*)-.*?-.*?(src)?(\.rpm)")

  for pkg_name in lines:
    match = bucket_sort_regex.match(pkg_name)
    control_count = control_count + 1

    pkg_key = match.group(1,2,3)

    if pkg_key not in buckets:
      buckets[pkg_key] = 0
    elif buckets[pkg_key] < int(num_versions_kept)-1:
      buckets[pkg_key] = buckets[pkg_key] + 1
    else:
      delete_list.append(pkg_name)

  return delete_list

parser = OptionParser()
(options, args) = parser.parse_args()

del_list = get_delete_list(*args)
print "Files to be removed:"
pprint_list(del_list)
map(lambda f:os.remove(os.path.join(args[0],f)), del_list)


