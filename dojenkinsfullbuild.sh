#
# Parameters:
#  name of the subdir that just got the SVN snapshot (e.g. ./dmlite)
#  filename of the mock config to be used relative to ./build/mock
#  destination repo full path e.g. /afs/cern.ch/project/gd/dms/www/lcgdm/repos/el6/x86_64/
#  password to get an afs token to the destination repo
#
#
# Fabrizio Furano (furano@cern.ch)
# June 2015

if [[ "$1" == "" || "$3" == ""  || "$4" == "" || "$5" == "" || "$6" == "" ]]; then
  echo "Usage $0 [prjdir] [fixed specfile (can be "")] [mock cfg filename] [gdrepo_path] [gdrepo_pwd] [is_custom_dist]"
  exit -1
fi

set -x
set -e

prjdir=$1
mockcfg=$3
destrepo=$4
pass=$5
module_customdist=$6
specloc=${prjdir}/$2

reltag="`date '+%Y%m%d.%H%M'`rc"
if [[ "${module_customdist}" == "true" ]]; then
  reltag=`date '+%Y%m%d.%H%M'`
fi

echo "Release tag is '${reltag}' ... good luck."


if [[ -z "$BUILD_NUMBER" ]]; then
  rootext=$RANDOM$RANDOM$RANDOM
else
  rootext="build$BUILD_NUMBER"
fi

echo "Root ext for unique builds is '${rootext}' ... best of luck."






echo "--------------------------------------------- Cleanup"
rm -rf RPMS SRPMS
rm -rf builddir
mkdir -p builddir
echo "---------------------------------------------"

echo "--------------------------------------------- Environment dump"
env
echo "---------------------------------------------"




echo "--------------------------------------------- Getting AFS token"

# authenticate with the service account for access
kinit gdrepo@CERN.CH <<EOF
${pass}
EOF
if [ $? -ne 0 ]; then
echo "Could not get AFS token for writing to the repository"
exit 1
fi
aklog
if [ $? -ne 0 ]; then
 echo "Could not convert kerberos to afs token"
 exit 1
fi

echo "---------------------------------------------"






#echo "--------------------------------------------- Mounting tmpfs"

#if [ "$(id -u)" != "0" ]; then
#  echo "The user is not root. Will continue without tmpfs"
#else
#  echo "Mounting tmpfs..."

#  # mount the tmpfs
#  mount | grep "tmpfs on /var/lib/mock"
#  if [[ ! $? == 0 ]]; then
#    mount -t tmpfs -o size=1536m tmpfs /var/lib/mock
#    echo "tmpfs created"
#  else
#    USE=`df /var/lib/mock | awk '{ print $5 }' | tail  -1 | sed 's/%//'`
#    if [[ $USE > 50 ]]; then
#      umount /var/lib/mock
#      mount -t tmpfs -o size=1536m tmpfs /var/lib/mock
#      echo "tmpfs was $USE % full. threshold is 50 %. remounted it."
#    fi
#  fi
#  echo "LS: `ls /var/lib/mock`"
#  echo "MOUNT: `mount`"
#  echo "DF: `df -h`"
#fi

#echo "---------------------------------------------"
















echo "---------- Creating SRPM ------------------------- listing of `pwd`"
ls -l
echo "---------------------------------------------"

cmd="./build/packager_rpm.sh ${specloc} ./${prjdir}  "
if [[ "x${reltag}" == "x" ]]; then
#if [[ "x${module_customdist}" == "xtrue" ]]; then
  cmd="./build/packager_rpm.sh ${specloc} ./${prjdir} ${reltag} "
fi

echo "Running: $cmd"
$cmd





echo "---------- Running mock build --------------- listing of `pwd`"
ls -l
echo "---------------------------------------------"

if [[ "x${reltag}" != "x" ]]; then
#if [[ "x${module_customdist}" == "xtrue" ]]; then
  # /usr/bin/mock -v -D "dist .${reltag}.el%{?rhel}" --configdir=`pwd`/build/mock/ -r `pwd`/build/mock/${mockcfg} --rebuild `pwd`/RPMS/*.src.rpm --resultdir=`pwd`/builddir

  /usr/bin/mock -v -D "dist .${reltag}.el%{?rhel}" --configdir=`pwd`/build/mock/ -r ${mockcfg} --uniqueext=${rootext} --yum-cmd "clean" "all"
  /usr/bin/mock -v -D "dist .${reltag}.el%{?rhel}" --configdir=`pwd`/build/mock/ -r ${mockcfg} --rebuild `pwd`/RPMS/*.src.rpm --resultdir=`pwd`/builddir --uniqueext=${rootext}
else

  /usr/bin/mock -v -D "dist .el%{?rhel}" --configdir=`pwd`/build/mock/ -r ${mockcfg} --uniqueext=${rootext} --yum-cmd "clean" "all"
  /usr/bin/mock -v -D "dist .el%{?rhel}" --configdir=`pwd`/build/mock/ -r ${mockcfg} --rebuild `pwd`/RPMS/*.src.rpm --resultdir=`pwd`/builddir --uniqueext=${rootext}
fi

#if [[ "${module_customdist}" == "true" ]]; then
	echo "---------- Cleanup AFS repos --------------- listing of builddir"
	ls -l builddir
	echo "---------------------------------------------"
	/usr/bin/python ./build/delete_old_package_versions.py ${destrepo} "" 2
#fi


echo "---------- Copying to AFS repos --------------- listing of builddir"
ls -l builddir
echo "---------------------------------------------"

./build/copy_to_repo.sh ${destrepo} `pwd`/builddir ${pass}


