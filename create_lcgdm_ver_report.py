#!/usr/bin/python
# -*- coding: utf-8 -*-

import subprocess


repo1 = "/afs/cern.ch/project/gd/www/dms/lcgdm/repos/release-candidate/el5/i386"
repo2 = "/afs/cern.ch/project/gd/www/dms/lcgdm/repos/release-candidate/el5/x86_64"
repo3 = "/afs/cern.ch/project/gd/www/dms/lcgdm/repos/release-candidate/el6/i386"
repo4 = "/afs/cern.ch/project/gd/www/dms/lcgdm/repos/release-candidate/el6/x86_64"
head1 = "Component"
head2 = "EL5-i386"
head3 = "EL5-x86_64"
head4 = "EL6-i386"
head5 = "EL6-x86_64"


def xeq(repo, tks, idx):
  
  cmd = "ls %s | grep 'src\.rpm' | sort | sed 's/\([a-z][0-9]*\)-\([0-9]\)/\\1 \\2/' | sed 's/\.el..*\.src\.rpm//'"
  
  # Execute the command
  #print (cmd % (repo1))
  v = subprocess.check_output(cmd % (repo), shell=True)

  # Split the result into lines
  lines = v.split( "\n" )

  # Loop on the lines, using the first token as key
  #print lines

  for l in lines:
    ll = l.split()
    #print ll
    if (len(ll) == 2):
      
      # put the version value that we have found in the given position
      # As the elements are sorted, we will always keep the latest version
      
      
      if (ll[0] in tks):
	arr = tks[ll[0]]
      else:
	arr = ['', '', '', '']
      
        
      arr[idx] = ll[1]
      tks[ll[0]] = arr
      


mytks = dict( [] )
xeq(repo1, mytks, 0)
xeq(repo2, mytks, 1)
xeq(repo3, mytks, 2)
xeq(repo4, mytks, 3)


# Print the column names
print head1, head2, head3, head4, head5

# Print the report
for k in mytks:
  print k, ' '.join( mytks[k] )