
#
# This script copies the artifacts of the build into the final repo
#
# Fabrizio Furano (furano@cern.ch)
# June 2015

if [[ "$1" == "" || "$2" == "" ]]; then
        echo "Usage $0 [dest_dir] [source_dir] [gdrepo_pwd] "
        exit -1
fi

set -x

dest_dir=$1
source_dir=$2
gdrepo_pwd=$3


tn=$(hostname).$$

# authenticate with the service account for access
kinit gdrepo@CERN.CH <<EOF
${gdrepo_pwd}
EOF
if [ $? -ne 0 ]; then
echo "Could not get AFS token for writing to the repository"
exit 1
fi
aklog
if [ $? -ne 0 ]; then
 echo "Could not convert kerberos to afs token"
 exit 1
fi

# prefer createrepo over createrepo_c
# because createrepo_c seems to have linking issues on some systems
CREATEREPO=`which createrepo_c`
which createrepo
if [[ $? == 0 ]]; then
  CREATEREPO=`which createrepo`
fi
if [[ "${CREATEREPO}x" == "x" ]]; then
  echo "Could not find command: createrepo (or createrepo_c)"
  exit 1
fi

cd ${dest_dir} || exit 1

cp ${source_dir}/*.rpm ${dest_dir}

counter=0
while [[ $counter < 3 ]] ; do
  counter=$((counter + 1))
  ret=0
  rm -rf $tn >& /dev/null
  mkdir $tn || exit 1
  mtime=$(stat -c "%Y" repodata 2>/dev/null)
  sleep 1
  cp -r repodata $tn
  $CREATEREPO --update -o $tn .
  ret=$?
  if [ $ret -ne 0 ]; then
    echo "error from createrepo_c"
    break
  fi
  mv repodata $tn/repodata- >& /dev/null
  mv -T $tn/repodata repodata >& /dev/null
  ret=$?
  if [ $ret -ne 0 ]; then
    continue
  fi
  m0time=$(stat -c "%Y" $tn/repodata- 2>/dev/null)
  if [ -z "$mtime" -a -z "$m0time" ]; then
    break
  fi
  if [ -n "$mtime" -a -n "$m0time" ]; then
    if [ $mtime -eq $m0time ]; then
      break
    fi
  fi
  echo "retrying"
done
echo "Exiting after $counter attempts"
rm -rf $tn >& /dev/null
exit $ret





