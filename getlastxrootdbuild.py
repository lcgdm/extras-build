#!/usr/bin/env python
#
# This script tells us what is the last successful xrootd build, taken from TeamCity
# (c) 2015 CERN IT/SDC
#
# Fabrizio Furano - furano@cern.ch
# Courtesy of Lukasz Janyst
#

import mechanize
import lxml.etree

br = mechanize.Browser()
response = br.open( "https://teamcity-dss.cern.ch:8443/guestAuth/app/rest/buildTypes/id:bt45/builds" )
#print "response: ", response
doc = lxml.etree.fromstring( response.read() )
#print "doc: ", doc

buildList = doc.xpath( "//build[@status='SUCCESS']" )
print buildList[0].attrib['id']


