#!/bin/bash
## help tool for generic packaging
# @author: Devresse Adrien

## vars
BASE_TMP=/tmp
FINAL_DIR="RPMS/"
set -x

TARBALL_FILTER="--exclude='.git' --exclude='.svn' --exclude='RPMS' --exclude='*.rpm'"
FOO="HELLO"

set -e

# takes the suggested location, otherwise generates a random
create_tmp_dir() {
	if [[ -z "$1" ]]; then
	  export TMP_DIR="$BASE_TMP/$RANDOM$RANDOM$RANDOM_$$"
	else
	  export TMP_DIR="$BASE_TMP/$1"
	fi
	mkdir -p $TMP_DIR
}

get_attrs_spec(){
	export SPEC_CONTENT="$(rpm -E "`cat $1`")"
	export PKG_VERSION="$( echo "$SPEC_CONTENT" | grep "Version:"  | sed 's@Version:\(.*\)@\1@g' | sed -e 's/^[ \t]*//')"
	export PKG_RELEASE="$( echo "$SPEC_CONTENT" | grep "Release:"  | sed 's@Release:\(.*\)@\1@g' | sed -e 's/%.*//' | sed -e 's/^[ \t]*//')"	
	export PKG_NAME="$(echo "$SPEC_CONTENT" | grep "Name:" | sed 's@Name:\(.*\)@\1@g' | sed -e 's/^[ \t]*//')"
	export SPEC_CONTENT="$(echo "$SPEC_CONTENT" | sed "s/%{name}/$PKG_NAME/g" | sed "s/%{version}/$PKG_VERSION/g" | sed "s/%{Release}/$PKG_RELEASE/g")"
	export PKG_SOURCE="$( echo "$SPEC_CONTENT" | grep "Source0:"  | sed 's@Source0:\(.*\)@\1@g' )"
	export PKG_SOURCE="$( echo $PKG_SOURCE | awk -F/ '{print $NF'})"
	if [[ -z "$PKG_SOURCE" ]]; then
	  export PKG_SOURCE="$PKG_NAME-$PKG_VERSION-$PKG_RELEASE"
	fi
	export SRC_NAME="$PKG_SOURCE"
	echo "res : $SRC_NAME $PKG_VERSION $PKG_NAME $PKG_SOURCE"
}

# src_dir, tarbal_filepath
create_tarball(){
	create_tmp_dir
	SRC_FOLDER="/$TMP_DIR/$PKG_NAME-$PKG_VERSION"
	mkdir -p "$SRC_FOLDER"
	echo "copy files..."
        cp -r $1/* $SRC_FOLDER/
	CURRENT_DIR=$PWD
	cd $TMP_DIR
	echo "copy files..."
	eval "tar -cvzf $2 $TARBALL_FILTER $PKG_NAME-$PKG_VERSION"
	echo "tarball result : $2 $TARBALL_FILTER "
	cd $CURRENT_DIR
	rm -rf $TMP_DIR
}

# specfiles_dir 
create_rpmbuild_env(){
	create_tmp_dir $TMP_DIR_RND
	export RPM_BUILD_DIR="$TMP_DIR"	
	mkdir -p $RPM_BUILD_DIR/RPMS $RPM_BUILD_DIR/SOURCES $RPM_BUILD_DIR/BUILD $RPM_BUILD_DIR/SRPMS $RPM_BUILD_DIR/SPECS $RPM_BUILD_DIR/tmp
	if [[ -d "$1" ]]; then
	  cp -R $1/* $RPM_BUILD_DIR/SPECS/
	else
	  cp -R $1 $RPM_BUILD_DIR/SPECS/
	fi
	
}

# specfiles_dir 
delete_rpmbuild_env(){
	echo "I should do: rm -rf $RPM_BUILD_DIR/"
	#rm -rf $RPM_BUILD_DIR/
	
}


# specfile
rpm_build_src_package(){
	echo "Begin the rpmbuild source call for spec file $1 .... disttag is -$2-"
	local OLD_DIR=$PWD
	local MACRO_TOPDIR="s  \"_topdir $RPM_BUILD_DIR\""
	cd $RPM_BUILD_DIR
	ls $PWD/SOURCES/

        if [[ -z "$2" ]]; then
          rpmbuild -bs --nodeps --define "_source_filedigest_algorithm md5" --define "_binary_filedigest_algorithm md5" --define "_topdir $RPM_BUILD_DIR" SPECS/$1
        else
          rpmbuild -bs --nodeps --define "dist $2" --define "_topdir $RPM_BUILD_DIR" --define "_source_filedigest_algorithm md5" --define "_binary_filedigest_algorithm md5" SPECS/$1
        fi

	cd $OLD_DIR
	echo "End the rpmbuild source call...."	
}



## main
if [[ -z "$1" || -z "$2" ]]; then
	echo "Usage $0 [spec_dir] [src_dir] [packageDistTrunk]"
	exit -1
fi

create_rpmbuild_env $1
mkdir -p SRPMS

# list spec file

if [ -d $1 ]; then
  echo " create RPMS for spec file directory: $1"
  for i in $1/*.spec 
  do
	echo " create RPMS for spec file : $i"
	get_attrs_spec $i
	echo "Source : $SRC_NAME"
	echo "Version : $PKG_VERSION"
	echo "Name: $PKG_NAME"	
	echo "create source tarball..."
	create_tarball $2 "$RPM_BUILD_DIR/SOURCES/$SRC_NAME"
	echo "TARBALL: $RPM_BUILD_DIR/SOURCES/$SRC_NAME"
	rpm_build_src_package `basename $i` $3 
  done
else
        echo " create RPMS for unique spec file : $1"
        get_attrs_spec $1
        echo "Source : $SRC_NAME"
        echo "Version : $PKG_VERSION"
        echo "Name: $PKG_NAME"  
        echo "create source tarball..."
        create_tarball $2 "$RPM_BUILD_DIR/SOURCES/$SRC_NAME"
        echo "TARBALL: $RPM_BUILD_DIR/SOURCES/$SRC_NAME"
        rpm_build_src_package `basename $1` $3
fi

echo "Current directory is: `pwd`"
echo "Copying artifacts to $FINAL_DIR"
mkdir -p  $FINAL_DIR
cp -vv $RPM_BUILD_DIR/SRPMS/* $FINAL_DIR
## clean everything
delete_rpmbuild_env
	
